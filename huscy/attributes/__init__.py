# major.minor.patch.release.number
# release must be one of alpha, beta, rc or final
VERSION = (1, 1, 0)

__version__ = '.'.join(str(x) for x in VERSION)

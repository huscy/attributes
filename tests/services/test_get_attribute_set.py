import pytest

from huscy.attributes import services
from huscy.attributes.models import AttributeSet
from huscy.pseudonyms.models import Pseudonym

pytestmark = pytest.mark.django_db


def test_get_existing_attribute_set(attribute_set, subject):
    result = services.get_attribute_set(subject)

    assert result == attribute_set


def test_create_empty_attribute_set_if_none_exists_yet_and_use_latest_attribute_schema(
        attribute_schema_v4, subject):
    assert AttributeSet.objects.count() == 0
    assert Pseudonym.objects.count() == 0

    attribute_set = services.get_attribute_set(subject)

    assert AttributeSet.objects.count() == 1
    assert Pseudonym.objects.count() == 1

    assert attribute_set.attributes == {}
    assert attribute_set.attribute_schema == attribute_schema_v4


def test_use_get_or_create_attribute_function(mocker, attribute_schema_v4, subject, pseudonym):
    spy = mocker.spy(services, '_get_or_create_attribute_set')

    services.get_attribute_set(subject)

    spy.assert_called_once_with(pseudonym.code)

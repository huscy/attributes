import pytest

from jsonschema.exceptions import ValidationError

from huscy.attributes.services import update_attribute_set

pytestmark = pytest.mark.django_db


def test_update_attribute_set(attribute_set):
    attributes = {
        'attribute1': 'foobar',
        'attribute2': 100.0,
    }

    update_attribute_set(attribute_set, attributes)

    attribute_set.refresh_from_db()
    assert attribute_set.attributes == {
        'attribute1': 'foobar',
        'attribute2': 100.0,
        'category1': {'attribute11': 25},
        'category2': {'attribute21': 0},
    }


def test_update_attributes_with_invalid_data(attribute_set):
    attributes = {
        'attribute2': 'invalid',
    }

    with pytest.raises(ValidationError) as e:
        update_attribute_set(attribute_set, attributes)

    assert str(e.value).startswith("'invalid' is not of type 'number'")


def test_update_for_nested_attributes(attribute_set):
    attributes = {
        'attribute1': 'new string',
        'category1': {'attribute11': 25},
        'category2': {'attribute22': 'foobar'}
    }

    update_attribute_set(attribute_set, attributes)

    attribute_set.refresh_from_db()
    assert attribute_set.attributes == {
        'attribute1': 'new string',
        'attribute2': 4.5,
        'category1': {
            'attribute11': 25,
        },
        'category2': {
            'attribute21': 0,
            'attribute22': 'foobar',
        },
    }

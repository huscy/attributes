import pytest

from huscy.attributes.models import AttributeSchema
from huscy.attributes.services import get_attribute_schema

pytestmark = pytest.mark.django_db


def test_get_initial_attribute_schema():
    attribute_schema = get_attribute_schema()
    assert attribute_schema.schema == {'type': 'object', 'properties': {}}


def test_get_latest_attribute_schema(attribute_schema_v4, schema_v4):
    attribute_schema = get_attribute_schema()
    assert attribute_schema.schema == schema_v4


def test_get_attribute_schema_by_version_number(attribute_schema_v4, schema_v2):
    '''
    To realise the test, a workaround was implemented to work with a specific version number,
    because with django_db_reset_sequences the primary keys would start counting again at 1, which
    leads to a conflict with the initial attribute schema.
    '''
    attribute_schema_versions = AttributeSchema.objects.order_by('id').values_list('id', flat=True)

    attribute_schema = get_attribute_schema(version=attribute_schema_versions[1])

    assert attribute_schema.schema == schema_v2


def test_get_attribute_schema_by_invalid_version_number():
    with pytest.raises(AttributeSchema.DoesNotExist):
        get_attribute_schema(version=2)

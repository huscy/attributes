from datetime import datetime

import pytest
from freezegun import freeze_time

from django.contrib.auth.models import Permission

from huscy.attributes import services
from huscy.attributes.models import AttributeSchema

pytestmark = pytest.mark.django_db


@pytest.fixture
def simple_schema():
    return {
        'type': 'object',
        'properties': {},
    }


@pytest.fixture
def schema_with_categories():
    return {
        'type': 'object',
        'properties': {
            'attribute1': {'type': 'string'},
            'category1': {
                'type': 'object',
                'properties': {
                    'attribute11': {'type': 'string'},
                },
            },
            'attribute2': {'type': 'number'},
            'category2': {
                'type': 'object',
                'properties': {
                    'attribute21': {'type': 'string'},
                    'attribute22': {'type': 'string'},
                }
            }
        }
    }


@freeze_time('2000-01-01T10:00:00')
def test_update_attribute_schema(mocker, simple_schema):
    create_spy = mocker.spy(services, '_create_attribute_category_permissions')
    delete_spy = mocker.spy(services, '_delete_orphaned_attribute_category_permissions')

    result = services.update_attribute_schema(simple_schema)

    assert isinstance(result, AttributeSchema)
    assert result.schema == simple_schema
    assert result.created_at == datetime(2000, 1, 1, 10)

    create_spy.assert_called_once_with(simple_schema)
    delete_spy.assert_called_once()


def test_create_attribute_category_permissions(mocker, schema_with_categories):
    read_spy = mocker.spy(services, '_create_attribute_category_read_permission')
    write_spy = mocker.spy(services, '_create_attribute_category_write_permission')

    assert not Permission.objects.filter(codename__contains='_attribute_category_').exists()

    services.update_attribute_schema(schema_with_categories)

    assert 4 == Permission.objects.filter(codename__contains='_attribute_category_').count()
    assert Permission.objects.filter(codename='change_attribute_category_category1').exists()
    assert Permission.objects.filter(codename='change_attribute_category_category2').exists()
    assert Permission.objects.filter(codename='view_attribute_category_category1').exists()
    assert Permission.objects.filter(codename='view_attribute_category_category2').exists()

    assert 2 == read_spy.call_count
    assert 2 == write_spy.call_count


def test_delete_orphaned_attribute_category_permissions(schema_with_categories, simple_schema):
    services.update_attribute_schema(schema_with_categories)

    assert 4 == Permission.objects.filter(codename__contains='_attribute_category_').count()

    services.update_attribute_schema(simple_schema)

    assert 0 == Permission.objects.filter(codename__contains='_attribute_category_').count()

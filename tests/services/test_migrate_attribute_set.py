import pytest

import jsonschema

from huscy.attributes.models import AttributeSet
from huscy.attributes.services import AttributeSetMigrationError, migrate_attribute_set


pytestmark = pytest.mark.django_db


@pytest.fixture
def attribute_set(attribute_schema_v2, pseudonym):
    return AttributeSet.objects.create(
        pseudonym=pseudonym.code,
        attributes={
            'attribute1': 'any string',
        },
        attribute_schema=attribute_schema_v2,
    )


def test_migrate_attribute_set_to_latest_attribute_schema(attribute_schema_v4, attribute_set):
    migrate_attribute_set(attribute_set)

    assert attribute_set.attribute_schema == attribute_schema_v4


def test_migrate_attribute_set_to_fix_version(attribute_schema_v3, attribute_schema_v4,
                                              attribute_set):

    migrate_attribute_set(attribute_set, attribute_schema_v3)

    assert attribute_set.attribute_schema == attribute_schema_v3


def test_downgrade_attribute_schema_raises_exception(attribute_schema_v3, attribute_schema_v4,
                                                     attribute_set):
    migrate_attribute_set(attribute_set)  # migrate to latest version

    with pytest.raises(AttributeSetMigrationError) as e:
        migrate_attribute_set(attribute_set, attribute_schema_v3)

    assert str(e.value) == ('New version for attribute schema must be greater than '
                            'current attribute schema version.')


def test_update_attributes(attribute_schema_v3, attribute_set):
    attributes = {
        'attribute1': 'any string',
        'attribute2': 4.5,
    }
    migrate_attribute_set(attribute_set, attribute_schema_v3, attributes=attributes)

    assert attribute_set.attributes == attributes


def test_update_invalid_attributes_raises_exception(attribute_schema_v3, attribute_set):
    attributes = {
        'attribute1': 2.2,
        'attribute2': 4.5,
    }
    with pytest.raises(jsonschema.exceptions.ValidationError) as e:
        migrate_attribute_set(attribute_set, attribute_schema_v3, attributes=attributes)

    assert str(e.value).startswith("2.2 is not of type 'string'")

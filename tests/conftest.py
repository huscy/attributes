import pytest
from model_bakery import baker

from huscy.attributes.models import AttributeSchema, AttributeSet
from huscy.attributes.services import update_attribute_schema
from huscy.pseudonyms.services import get_or_create_pseudonym


@pytest.fixture
def user(django_user_model):
    return django_user_model.objects.create_user(username='user', password='password',
                                                 first_name='Jim', last_name='Panse')


@pytest.fixture
def staff_user(user):
    user.is_staff = True
    user.save()
    return user


@pytest.fixture
def schema_v2():
    return {
        'type': 'object',
        'properties': {
            'attribute1': {'type': 'string'},
        }
    }


@pytest.fixture
def schema_v3():
    return {
        'type': 'object',
        'properties': {
            'attribute1': {'type': 'string'},
            'attribute2': {'type': 'number'},
        }
    }


@pytest.fixture
def schema_v4():
    return {
        'type': 'object',
        'properties': {
            'attribute1': {'type': 'string'},
            'attribute2': {'type': 'number'},
            'category1': {
                'type': 'object',
                'properties': {
                    'attribute11': {'type': 'integer'},
                },
            },
            'category2': {
                'type': 'object',
                'properties': {
                    'attribute21': {'type': 'integer'},
                    'attribute22': {'type': 'string'},
                },
            },
        },
        'additionalProperties': False,
    }


@pytest.fixture
def attribute_schema_v2(schema_v2):
    return AttributeSchema.objects.create(schema=schema_v2)


@pytest.fixture
def attribute_schema_v3(attribute_schema_v2, schema_v3):
    return AttributeSchema.objects.create(schema=schema_v3)


@pytest.fixture
def attribute_schema_v4(attribute_schema_v3, schema_v4):
    return update_attribute_schema(schema_v4)


@pytest.fixture
def subject():
    return baker.make('subjects.Subject')


@pytest.fixture
def pseudonym(subject):
    return get_or_create_pseudonym(subject, 'attributes.attributeset')


@pytest.fixture
def attribute_set(attribute_schema_v4, pseudonym):
    return AttributeSet.objects.create(
        pseudonym=pseudonym.code,
        attributes={
            'attribute1': 'any string',
            'attribute2': 4.5,
            'category1': {
                'attribute11': 25,
            },
            'category2': {
                'attribute21': 0,
            },
        },
        attribute_schema=attribute_schema_v4,
    )

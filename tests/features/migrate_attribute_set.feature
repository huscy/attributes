Feature: migrate attribute set

	Scenario: migrate attribute set as admin user
		Given I am admin user
		And There's one attribute set with attribute schema version 2
		And The latest attribute schema is in version 4
		When I try to migrate an attribute set to latest attribute schema version
		Then I get status code 200
		And The attribute schema version of the attribute set is in version 4

	Scenario: migrate attribute set as staff user
		Given I am staff user
		And There's one attribute set with attribute schema version 2
		And The latest attribute schema is in version 4
		When I try to migrate an attribute set to latest attribute schema version
		Then I get status code 403
		And The attribute schema version of the attribute set is in version 2

	Scenario: migrate attribute set as normal user with migrate_attributeset permission
		Given I am normal user
		And I have migrate_attributeset permission
		And There's one attribute set with attribute schema version 2
		And The latest attribute schema is in version 4
		When I try to migrate an attribute set to latest attribute schema version
		Then I get status code 200
		And The attribute schema version of the attribute set is in version 4

	Scenario: migrate attribute set as normal user
		Given I am normal user
		And There's one attribute set with attribute schema version 2
		And The latest attribute schema is in version 4
		When I try to migrate an attribute set to latest attribute schema version
		Then I get status code 403
		And The attribute schema version of the attribute set is in version 2

	Scenario: migrate attribute set as anonymous user
		Given I am anonymous user
		And There's one attribute set with attribute schema version 2
		And The latest attribute schema is in version 4
		When I try to migrate an attribute set to latest attribute schema version
		Then I get status code 403
		And The attribute schema version of the attribute set is in version 2

	Scenario: migrate attribute set to fix version of attribute schema version
		Given I am admin user
		And There's one attribute set with attribute schema version 2
		And The latest attribute schema is in version 4
		When I try to migrate an attribute set to attribute schema version 3
		Then I get status code 200
		And The attribute schema version of the attribute set is in version 3

	Scenario: migrate attribute set to fix attribute schema version and update attributes
		Given I am admin user
		And There's one attribute set with attribute schema version 2
		And The latest attribute schema is in version 4
		When I try to migrate an attribute set to attribute schema version 3 and update attributes
		Then I get status code 200
		And The attribute schema version of the attribute set is in version 3
		And The attributes were updated to version 3

	Scenario: migrate attribute set to latest attribute schema version und update attributes
		Given I am admin user
		And There's one attribute set with attribute schema version 2
		And The latest attribute schema is in version 4
		When I try to migrate an attribute set to latest attribute schema version and update attributes
		Then I get status code 200
		And The attribute schema version of the attribute set is in version 4
		And The attributes were updated to version 4

	Scenario: migrate attribute set to same attribute schema version raises exception
		Given I am admin user
		And There's one attribute set with attribute schema version 4
		When I try to migrate an attribute set to latest attribute schema version
		Then I get status code 400

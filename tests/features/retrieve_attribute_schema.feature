Feature: retrieve attribute schema

	Scenario: retrieve attribute schema as admin user
		Given I am admin user
		When I try to retrieve an attribute schema
		Then I get status code 200

	Scenario: retrieve attribute schema as staff user
		Given I am staff user
		When I try to retrieve an attribute schema
		Then I get status code 200

	Scenario: retrieve attribute schema as normal user
		Given I am normal user
		When I try to retrieve an attribute schema
		Then I get status code 200

	Scenario: retrieve attribute schema as anonymous user
		Given I am anonymous user
		When I try to retrieve an attribute schema
		Then I get status code 403

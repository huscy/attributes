Feature: retrieve attribute set

	Scenario: retrieve attribute set as admin user
		Given I am admin user
		When I try to retrieve an attribute set
		Then I get status code 200

	Scenario: retrieve attribute set as staff user
		Given I am staff user
		When I try to retrieve an attribute set
		Then I get status code 403

	Scenario: retrieve attribute set as normal user with view_attributeset
		Given I am normal user
		And I have view_attributeset permission
		When I try to retrieve an attribute set
		Then I get status code 200

	Scenario: retrieve attribute set as normal user
		Given I am normal user
		When I try to retrieve an attribute set
		Then I get status code 403

	Scenario: retrieve attribute set as anonymous user
		Given I am anonymous user
		When I try to retrieve an attribute set
		Then I get status code 403

	Scenario: filter all categories if user has no category permission
		Given I am normal user
		And There's one attribute set with attribute schema version 4
		And I have view_attributeset permission
		When I try to retrieve an attribute set
		Then I can see the non-categorized attributes
		And I cannot see category 1 attributes
		And I cannot see category 2 attributes

	Scenario: filter all categories for which the user has no category permission
		Given I am normal user
		And There's one attribute set with attribute schema version 4
		And I have view_attributeset permission
		And I have view_attribute_category_category1 permission
		When I try to retrieve an attribute set
		Then I can see the non-categorized attributes
		And I can see category 1 attributes
		And I cannot see category 2 attributes

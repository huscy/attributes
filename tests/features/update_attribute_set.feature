Feature: update attribute set

	Scenario: update attribute set as admin user
		Given I am admin user
		When I try to update an attribute set
		Then I get status code 200

	Scenario: update attribute set as staff user
		Given I am staff user
		When I try to update an attribute set
		Then I get status code 403

	Scenario: update attribute set as normal user with change_attributeset permission
		Given I am normal user
		And I have change_attributeset permission
		When I try to update an attribute set
		Then I get status code 403

	Scenario: update attribute set as normal user with change_subject permission
		Given I am normal user
		And I have change_subject permission
		When I try to update an attribute set
		Then I get status code 403

	Scenario: update attribute set as normal user with change_attributeset and change_subject permissions
		Given I am normal user
		And I have change_attributeset permission
		And I have change_subject permission
		When I try to update an attribute set
		Then I get status code 200

	Scenario: update attribute set as normal user
		Given I am normal user
		When I try to update an attribute set
		Then I get status code 403

	Scenario: update attribute set as anonymous user
		Given I am anonymous user
		When I try to update an attribute set
		Then I get status code 403

	Scenario: update attribute category 2 with change_attribute_category_category2 permission
		Given I am normal user
		And There's one attribute set with attribute schema version 4
		And I have change_attributeset permission
		And I have change_attribute_category_category2 permission
		And I have change_subject permission
		When I try to update attributes within attribute category 2
		Then I get status code 200

	Scenario: raise permission denied when updating attribute category 2 without change_attribute_category_category2 permission
		Given I am normal user
		And There's one attribute set with attribute schema version 4
		And I have change_attributeset permission
		And I have change_subject permission
		When I try to update attributes within attribute category 2
		Then I get status code 403

	Scenario: raise permission denied when updating attribute category 2 with change_attribute_category_category1 permission
		Given I am normal user
		And There's one attribute set with attribute schema version 4
		And I have change_attributeset permission
		And I have change_attribute_category_category1 permission
		And I have change_subject permission
		When I try to update attributes within attribute category 2
		Then I get status code 403

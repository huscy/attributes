Feature: update attribute schema

	Scenario: update attribute schema as admin user
		Given I am admin user
		When I try to update an attribute schema
		Then I get status code 200

	Scenario: update attribute schema as staff user
		Given I am staff user
		When I try to update an attribute schema
		Then I get status code 403

	Scenario: update attribute schema as normal user with change_attributeschema permission
		Given I am normal user
		And I have change_attributeschema permission
		When I try to update an attribute schema
		Then I get status code 200

	Scenario: update attribute schema as normal user
		Given I am normal user
		When I try to update an attribute schema
		Then I get status code 403

	Scenario: update attribute schema as anonymous user
		Given I am anonymous user
		When I try to update an attribute schema
		Then I get status code 403
